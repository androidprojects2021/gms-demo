package com.example.gms_demo.model.response.employee

import com.google.gson.annotations.SerializedName

data class ResponseEmployeeList(

	@field:SerializedName("employees")
	val employees: List<EmployeesItem>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class EmployeesItem(

	@field:SerializedName("employee_id")
	val employeeId: String? = null,

	@field:SerializedName("employee_name")
	val employeeName: String? = null,

	@field:SerializedName("dept_id")
	val deptId: String? = null,

	var isSelected: Boolean? = false
)
