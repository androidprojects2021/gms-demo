package com.example.gms_demo.model.qrresult

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ImageXML(

    @field:SerializedName("cte")
    val cte: Cte? = null ,

    @field:SerializedName("DS")
    val ds: String? = null

): Serializable
