package com.example.gms_demo.model.response.purpose

import com.google.gson.annotations.SerializedName

data class ResponsePurposeList(

	@field:SerializedName("details")
	val details: ArrayList<DetailsItem>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DetailsItem(

	@field:SerializedName("p_v_id")
	val pVId: String? = null,

	@field:SerializedName("purpose")
	val purpose: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	var isSelected: Boolean? = false
)
