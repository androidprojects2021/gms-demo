package com.example.gms_demo.model.fcm_payload;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class fcm_payload implements Serializable {
    @SerializedName("userdetail")
    private userdetails userdetail;

    public userdetails getUserdetail(){
        return userdetail;
    }
}
