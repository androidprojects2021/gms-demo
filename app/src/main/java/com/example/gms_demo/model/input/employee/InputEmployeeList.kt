package com.example.gms_demo.model.input.employee

import com.google.gson.annotations.SerializedName

data class InputEmployeeList(

	@field:SerializedName("office_id")
	val officeId: String? = null,

	@field:SerializedName("dept_id")
	val deptId: String? = null
)
