package com.example.gms_demo.model.qrresult

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Cte(

    @field:SerializedName("v1")
    val v1: String? = null,

    @field:SerializedName("v2")
    val v2: String? = null,

    @field:SerializedName("v3")
    val v3: String? = null,

    @field:SerializedName("v4")
    val v4: String? = null,

    @field:SerializedName("v5")
    val v5: String? = null,

    @field:SerializedName("v6")
    val v6: String? = null,

    @field:SerializedName("v7")
    val v7: String? = null,

    @field:SerializedName("v8")
    val v8: String? = null,

    @field:SerializedName("v9")
    val v9: String? = null,

    @field:SerializedName("v10")
    val v10: String? = null,

    @field:SerializedName("v11")
    val v11: String? = null,

    @field:SerializedName("v12")
    val v12: String? = null,

    @field:SerializedName("v13")
    val v13: String? = null


) : Serializable
