package com.example.gms_demo.model.response.dept

import com.google.gson.annotations.SerializedName

data class ResponseDepartmentsList(

	@field:SerializedName("departments")
	val departments: ArrayList<DepartmentsItem>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DepartmentsItem(

	@field:SerializedName("dept_name")
	val deptName: String? = null,

	@field:SerializedName("dept_id")
	val deptId: String? = null,

	var isSelected: Boolean? = false
)
