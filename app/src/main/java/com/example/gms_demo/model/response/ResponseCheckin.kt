package com.example.gms_demo.model.response

import com.google.gson.annotations.SerializedName

data class ResponseCheckin(

	@field:SerializedName("details")
	val details: Details? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Details(

	@field:SerializedName("office_id")
	val officeId: String? = null,

	@field:SerializedName("office_name")
	val officeName: String? = null,

	@field:SerializedName("code")
	val code: String? = null
)
