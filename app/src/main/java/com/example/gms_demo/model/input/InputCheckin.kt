package com.example.gms_demo.model.input

import com.google.gson.annotations.SerializedName

data class InputCheckin(

	@field:SerializedName("code")
	val code: String? = null
)
