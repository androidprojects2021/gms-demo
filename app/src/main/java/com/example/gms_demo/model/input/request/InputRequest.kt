package com.example.gms_demo.model.input.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class InputRequest(

    @field:SerializedName("office_id")
    val officeId: String? = null,

    @field:SerializedName("device_id")
    val deviceId: String? = null,

    @field:SerializedName("purpose")
    var purpose: String? = null,

    @field:SerializedName("reason")
    val reason: String? = null,

    @field:SerializedName("employee_id")
    val employeeId: String? = null,

    @field:SerializedName("mobile")
    var mobile: String? = null,

    @field:SerializedName("device_type")
    val deviceType: String? = null,

    @field:SerializedName("dept_id")
    val deptId: String? = null,

    @field:SerializedName("qr_detail")
    var qr_detail: QRDetail? = null

) : Serializable

data class QRDetail(
    @SerializedName("v1")
    var v1: String? = null,

    @SerializedName("v2")
    var v2: String? = null,

    @SerializedName("v3")
    var v3: String? = null,

    @SerializedName("v4")
    var v4: String? = null,

    @SerializedName("v5")
    var v5: String? = null,


    @SerializedName("v6")
    var v6: String? = null,

    @SerializedName("v7")
    var v7: String? = null,

    @SerializedName("v8")
    var v8: String? = null,

    @SerializedName("v9")
    var v9: String? = null,

    @SerializedName("v10")
    var v10: String? = null,

    @SerializedName("v11")
    var v11: String? = null,

    @SerializedName("v12")
    var v12: String? = null,

    @SerializedName("v13")
    var v13: String? = null


)