package com.example.gms_demo.model.input.dept

import com.google.gson.annotations.SerializedName

data class InputDepartmentList(

	@field:SerializedName("office_id")
	val officeId: String? = null
)
