package com.example.gms_demo.model.qrresult

import com.google.gson.annotations.SerializedName

data class QrData(
    @field:SerializedName("imageXML")
    val imageXML: ImageXML? = null
)
