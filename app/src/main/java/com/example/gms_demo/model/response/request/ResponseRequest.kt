package com.example.gms_demo.model.response.request

import com.google.gson.annotations.SerializedName

data class ResponseRequest(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("responce_code")
	val responceCode: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("register_id")
	val registerId: String? = null
)
