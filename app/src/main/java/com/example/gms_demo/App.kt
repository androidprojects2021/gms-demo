package com.example.gms_demo

import android.app.Application
import com.example.gms_demo.network.ApiClient
import com.example.gms_demo.utils.NetworkConstants
import com.example.gms_demo.utils.SharedPrefs


/*   Created By:
 Reema 03/11/2021
 */
class App: Application() {

    val sharedPrefs = SharedPrefs
   val retrofit = ApiClient.getRetrofit(NetworkConstants.BaseUrl)

    override fun onCreate() {
        super.onCreate()
        SharedPrefs.getInstance(applicationContext)
    }



}