package com.example.gms_demo.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.gms_demo.model.input.request.InputRequest
import com.example.gms_demo.model.response.Details
import com.google.gson.Gson

object SharedPrefs {


    private lateinit var sharedPreferences: SharedPreferences

    private const val PREF_NAME = "prefName"
    private const val USER_DETAILS = "userDetails"
    private const val REQUEST_DETAILS = "requestDetails"
    private const val FCM_TOKEN = "token"
    private const val PURPOSE = "purpose"
    private const val P_REASON = "p_reason"
    private const val PHONE = "phone"
    private const val DEPARTMENT_ID = "dept_id"
    private const val EMPLOYEE_ID = "employee_id"
    private const val Q_ID = "q_id"

    fun clearSession(context: Context) {
        val editor =
            context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.apply()
    }

    fun getInstance(context: Context) {
        sharedPreferences =
            context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    var user_details: Details?
        get() = Gson().fromJson(sharedPreferences.getString(USER_DETAILS, ""), Details::class.java)
        set(id) = sharedPreferences.edit().putString(USER_DETAILS, Gson().toJson(id)).apply()


    var Firebasetoken: String?
        get() = sharedPreferences.getString(FCM_TOKEN, "")
        set(id) = sharedPreferences.edit().putString(FCM_TOKEN, id).apply()

    var purpose: String?
        get() = sharedPreferences.getString(PURPOSE, "")
        set(value) = sharedPreferences.edit().putString(PURPOSE, value).apply()

    var reason: String?
        get() = sharedPreferences.getString(P_REASON, "")
        set(value) = sharedPreferences.edit().putString(P_REASON, value).apply()

    var phone: String?
        get() = sharedPreferences.getString(PHONE, "")
        set(value) = sharedPreferences.edit().putString(PHONE, value).apply()

    var dept_id: String?
        get() = sharedPreferences.getString(DEPARTMENT_ID, "")
        set(value) = sharedPreferences.edit().putString(DEPARTMENT_ID, value).apply()

    var employee_id: String?
        get() = sharedPreferences.getString(EMPLOYEE_ID, "")
        set(value) = sharedPreferences.edit().putString(EMPLOYEE_ID, value).apply()

    var Q_id: String?
        get() = sharedPreferences.getString(Q_ID, "")
        set(value) = sharedPreferences.edit().putString(Q_ID, value).apply()


}