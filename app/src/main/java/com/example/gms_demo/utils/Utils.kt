package com.example.gms_demo.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.RippleDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.text.format.DateUtils
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.TextView
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*





fun isOnline(context: Context): Boolean {

    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false

}






fun getPressedColorSelector(normalColor: Int, pressedColor: Int): ColorStateList {
    return ColorStateList(
            arrayOf(intArrayOf(android.R.attr.state_pressed), intArrayOf(android.R.attr.state_focused), intArrayOf(android.R.attr.state_activated), intArrayOf()),
            intArrayOf(pressedColor, pressedColor, pressedColor, normalColor)
    )
}

fun getColorDrawableFromColor(color: Int): ColorDrawable {
    return ColorDrawable(color)
}



fun emailValidate(target: CharSequence?): Boolean {
    return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
}

fun isPasswordValid(password: String): Boolean {

    return password.length >= 6
}

fun isMobileNumbervalid(mob: String): Boolean {
    return mob.length >= 10
}





fun <T> stringToModel(data: String, model: Class<T>): T {
    return Gson().fromJson(data, model)
}

fun <T> modelToString(model: T): String {
    return Gson().toJson(model)
}

fun getDay(time: Long): String {
    val date = Date(time)
    val sdf = SimpleDateFormat("E", Locale.ENGLISH)
    return sdf.format(date)
}

fun animateTextView(initialValue: Float, finalValue: Float, textview: TextView) {

    val valueAnimator = ValueAnimator.ofFloat(initialValue, finalValue)
    valueAnimator.duration = 1500

    valueAnimator.addUpdateListener { animate ->
        textview.text = animate.animatedValue.toString()
    }
    valueAnimator.start()
}

public fun revealShow(dialogView: View, b: Boolean, fab: View) {

    val view = dialogView

    val w = view.width
    val h = view.height

    val endRadius = Math.hypot(w.toDouble(), h.toDouble()).toInt()

    val cx = (fab.x + fab.width / 2).toInt()
    val cy = fab.y.toInt() + fab.height + 56

    if (b) {
        val revealAnimator = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, endRadius.toFloat())
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        }

        view.visibility = View.VISIBLE
        revealAnimator.duration = 700
        revealAnimator.start()

    } else {

        val anim = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewAnimationUtils.createCircularReveal(view, cx, cy, endRadius.toFloat(), 0f)
        } else {
            TODO("VERSION.SDK_INT < LOLLIPOP")
        }

        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)

                view.visibility = View.INVISIBLE
            }
        })
        anim.duration = 700
        anim.start()
    }
}

fun getTimeago(timeStamp: Long): String {

    // Converting timestamp into x ago format

    val timeAgo = DateUtils.getRelativeTimeSpanString(
            java.lang.Long.parseLong(timeStamp.toString()),
            System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS)

    return timeAgo.toString()
}

fun getTimeStamp(date: String): Boolean {

    val bool: Boolean

    val formatter = SimpleDateFormat("YYYY-MM-dd", Locale.UK)
    val date1 = formatter.parse(date)

    bool = System.currentTimeMillis() > date1.time

    return bool
}

fun getDate(booking_date: String): Boolean {

    val date = Date(System.currentTimeMillis())
    val sdf = SimpleDateFormat("YYYYMMdd", Locale.ENGLISH)
    val newDate = booking_date.replace("-", "")

    Log.e("Today", sdf.format(date))
    Log.e("Today", newDate)

    return sdf.format(date).toInt() + 1 >= newDate.toInt()
}

fun getFormattedDate(booking_date: String): String {

    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    val outputFormat = SimpleDateFormat("dd MMM,EEE", Locale.ENGLISH)
    return outputFormat.format(inputFormat.parse(booking_date))
}

class UrlConstants(
        var ACCESS_CODE: String = "AVNC02FJ87BJ11CNJB",
        var MERCHANT_ID: String = "45170",
        var REDIRECT_URL: String = "https://www.offermaids.com/ccavenue-pay/ccavResponseHandler.php",
        var CANCEL_URL: String = "https://secure.ccavenue.ae/transaction/jsp/ResponseHandler_43551.jsp",
        var RSA_URL: String = "https://www.offermaids.com/ccavenue-pay/GetRSA.php"
)









