package com.example.gms_demo.service


import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.gms_demo.App
import com.example.gms_demo.model.fcm_payload.fcm_payload
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson



class FirebaseMessagingService : FirebaseMessagingService() {
    private var TAG = "MyFirebaseMessagingService"
    private val prefs = App().sharedPrefs

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "Token : " + token)
        Log.d("Firebase Token", "clickListen - token: " + token)

        prefs.Firebasetoken = token
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        Log.d(TAG, "message receive : " + remoteMessage.from)

        //Verify if the message contains data
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data : " + remoteMessage.data)
        }


        val data = remoteMessage.data["payload"]

        val clickAction = remoteMessage.notification!!.clickAction

        val fcmPayout: fcm_payload = Gson().fromJson(data, fcm_payload::class.java)

        showNotification(
            remoteMessage.notification!!.title, remoteMessage.notification!!
                .body, fcmPayout, clickAction
        )


    }

    private fun showNotification(title: String?, body: String?, fcmPayout: fcm_payload, clickAction: String?) {
        if (clickAction != null) {
            if (clickAction == "com.gms_fcm__payload") {
                val intent = Intent(clickAction)
                intent.putExtra("data", fcmPayout.userdetail)
                val localBroadcastReceiver = LocalBroadcastManager.getInstance(applicationContext)
                localBroadcastReceiver.sendBroadcast(intent)
            }
        }
    }


}