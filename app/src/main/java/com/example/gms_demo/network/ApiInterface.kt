package com.example.gms_demo.network

import com.example.gms_demo.model.input.InputCheckin
import com.example.gms_demo.model.input.dept.InputDepartmentList
import com.example.gms_demo.model.input.employee.InputEmployeeList
import com.example.gms_demo.model.input.request.InputRequest
import com.example.gms_demo.model.response.ResponseCheckin
import com.example.gms_demo.model.response.dept.ResponseDepartmentsList
import com.example.gms_demo.model.response.employee.ResponseEmployeeList
import com.example.gms_demo.model.response.purpose.ResponsePurposeList
import com.example.gms_demo.model.response.request.ResponseRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/*   Created By:
 Reema 03/11/2021
 */
interface ApiInterface {

    @POST("api/building_login")
    suspend fun checkincall(@Body inputCheckin: InputCheckin): ResponseCheckin

    @GET("api/purpose_of_visit")
    suspend fun getpurpose_list():ResponsePurposeList

    @POST("api/list_departments")
    suspend fun getDepartments(@Body inputDepartmentList: InputDepartmentList):ResponseDepartmentsList

    @POST("api/list_employees")
    suspend fun getEmployees(@Body inputEmployeeList: InputEmployeeList): ResponseEmployeeList

    @POST("api/user_register_new")
    suspend fun submitrequest(@Body inputRequest: InputRequest):ResponseRequest
}

