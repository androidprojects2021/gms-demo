package com.example.gms_demo.ui.fragment.dept

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.response.dept.DepartmentsItem
import com.example.gms_demo.model.response.purpose.DetailsItem

class DepartmentAdapter(
    val requireContext: Context,
    val deptlist: ArrayList<DepartmentsItem>,
    val onItemSelected: (DepartmentsItem) -> Unit
) :
    RecyclerView.Adapter<DepartmentAdapter.PlaceViewHolder>() {
    private val prefs = App().sharedPrefs

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DepartmentAdapter.PlaceViewHolder {
        return PlaceViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.custom_purpose_item_view,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DepartmentAdapter.PlaceViewHolder, position: Int) {
        holder.textviewpurpose.text = deptlist.get(position).deptName

        if (prefs.dept_id.equals(deptlist.get(position).deptId)) {
            deptlist.get(position).isSelected = true
        }



        if (deptlist.get(position).isSelected == true) {
            holder.textviewpurpose.setTextColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.white
                )
            )
            holder.cardview_purpose.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.red
                )
            )
        } else {
            holder.textviewpurpose.setTextColor(ContextCompat.getColor(requireContext, R.color.red))
            holder.cardview_purpose.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.white
                )
            )
        }

        holder.cardview_purpose.setOnClickListener {

            for (i in deptlist.indices) {
                deptlist.get(i).isSelected = false
            }
            deptlist.get(position).isSelected = true

            notifyDataSetChanged()
            onItemSelected.invoke(deptlist.get(position))
        }


    }

    override fun getItemCount(): Int {
        return deptlist.size
    }

    inner class PlaceViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var textviewpurpose = itemView.findViewById<TextView>(R.id.textviewpurpose)
        var cardview_purpose = itemView.findViewById<CardView>(R.id.cardview_purpose)

    }
}