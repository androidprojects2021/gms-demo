package com.example.gms_demo.ui.fragment.dept

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.input.request.InputRequest
import com.example.gms_demo.model.response.dept.DepartmentsItem
import com.example.gms_demo.utils.Loader
import com.example.gms_demo.utils.isOnline
import kotlinx.android.synthetic.main.fragment_department.*


class DepartmentFragment : Fragment() {

    private val prefs = App().sharedPrefs

    private lateinit var departmentViewModel: DepartmentViewModel
    var inputRequest: InputRequest? = null
    lateinit var departmentAdapter: DepartmentAdapter
    private var deptlist: Array<String> =
        arrayOf("Dept 1", "Dept 2", "Dept 3", "Dept 4", "Dept 5", "Dept 6", "Dept 7")

    private var Departmentlist = ArrayList<DepartmentsItem>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_department, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        departmentViewModel = ViewModelProvider(this).get(DepartmentViewModel::class.java)



        inputRequest = InputRequest()

        init()
        onclick()
        onObserve()
    }


    fun init() {

        val bundle = this.arguments
        if (bundle != null) {
            inputRequest = bundle.getSerializable("InputRequest") as InputRequest
        }

        val layoutManager = GridLayoutManager(requireContext(), 4)
        recycleView_department.layoutManager = layoutManager


        if (isOnline(requireContext())) {
/*
            Loader.showLoader(requireContext())
*/
            departmentViewModel.getdepartmentlist(prefs.user_details?.officeId)
            Log.d("TAG", "officeid : " + prefs.user_details?.officeId)
        } else {
            Toast.makeText(
                requireContext(),
                "Please connect to internet",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun onclick() {
        dept_next.setOnClickListener {
            if (isValid()) {
                findNavController().navigate(
                    R.id.action_DepartmentFragment_to_EmployeeListFragment,
                )
            }
        }
    }

    private fun isValid(): Boolean {

        if (prefs.dept_id.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please select department", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun onObserve() {
        departmentViewModel.departmentlistResponse.observe(viewLifecycleOwner, Observer {
/*
            Loader.hideLoader()
*/
            if (it.status.equals("success")) {

                if (it.departments != null) {
                    Departmentlist.clear()
                    Departmentlist.addAll(it.departments)

                    departmentAdapter = DepartmentAdapter(requireContext(), Departmentlist) {
                        prefs.dept_id = it.deptId
                    }
                    recycleView_department.adapter = departmentAdapter
                }
            } else {
                Toast.makeText(
                    requireContext(),
                    "something went wrong",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

}