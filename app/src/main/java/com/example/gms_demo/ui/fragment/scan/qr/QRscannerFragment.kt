package com.example.gms_demo.ui.fragment.scan.qr

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import com.example.gms_demo.R
import com.example.gms_demo.model.qrresult.QrData
import com.google.android.gms.common.util.CollectionUtils.listOf
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Detector.Detections
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.zxing.BarcodeFormat
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import kotlinx.android.synthetic.main.fragment_q_rscanner.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception
import java.util.*


class QRscannerFragment : Fragment() {
    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private val REQUEST_CAMERA_PERMISSION = 201
    private var intentData = ""
    var REQUESTCODE = 111

    private lateinit var codeScanner: CodeScanner


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_q_rscanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
        } else {
            startScanning()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                requireActivity().finish()

            }
        })
    }

    override fun onResume() {
        super.onResume()
       // initialiseDetectorsAndSources()
        if(::codeScanner.isInitialized) {
            codeScanner?.startPreview()
        }
    }

    override fun onPause() {
        super.onPause()
       // cameraSource?.release()
        if(::codeScanner.isInitialized) {
            codeScanner?.releaseResources()
        }
    }



    private fun startScanning() {
        // Parameters (default values)
        codeScanner = CodeScanner(requireContext(), scanner_view)
        codeScanner.camera = CodeScanner.CAMERA_FRONT // or CAMERA_FRONT or specific camera id
        codeScanner.formats = listOf(BarcodeFormat.QR_CODE) //.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.CONTINUOUS // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {

            activity?.runOnUiThread {
               // Toast.makeText(requireContext(), "Scan result:-  ${it.text}", Toast.LENGTH_LONG).show()
                copyToClipBoard(it.text)
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            activity?.runOnUiThread {
                Toast.makeText(requireContext(), "Camera initialization error:-  ${it.message}",
                    Toast.LENGTH_LONG).show()
            }
        }

        scanner_view.setOnClickListener {
            codeScanner.startPreview()
        }
    }




    private fun initialiseDetectorsAndSources() {
        barcodeDetector = BarcodeDetector.Builder(requireContext()).setBarcodeFormats(Barcode.ALL_FORMATS).build()

        cameraSource = CameraSource.Builder(requireContext(), barcodeDetector)
            .setFacing(1)  // 0 - Backcam , 1 - Frontcam
            .setRequestedPreviewSize(300, 300).setAutoFocusEnabled(true).build()

/*
        surfaceView.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                Log.d("Scan1", "surfaceCreated: ")
                openCamera()
            }

            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource?.stop()
            }
        })
*/


        barcodeDetector?.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {

            }

            override fun receiveDetections(detections: Detections<Barcode>) {

                try {


                    Log.d("Scan2", "surfaceCreated: " + detections)

                    val barcodes = detections.detectedItems
                    Log.d("Scan3", "surfaceCreated: " + barcodes!!.valueAt(0).displayValue)

                    if (barcodes.size() > 0) {

                        setBarCode(barcodes)
                    }

                }catch (e:Exception){}
            }
        })


    }

    private fun setBarCode(barCode: SparseArray<Barcode>?) {
       /* txtBarcodeValue.post(Runnable {
            intentData = barCode!!.valueAt(0).displayValue
            txtBarcodeValue.setText(intentData)
            copyToClipBoard(intentData)
        })*/
    }

    private fun copyToClipBoard(intentData: String) {



        try {

            val xmlToJson =  XmlToJson.Builder(intentData).build()
            val data = Gson().fromJson(xmlToJson.toString(), QrData::class.java)
            Log.d("Scan result", "receiveDetections: "+data)




            if (data != null && data.imageXML != null && data.imageXML.cte != null) {
                if (!data.imageXML.cte.v3?.isEmpty()!!) {


                    val intent = Intent()
                    intent.putExtra("barCode", data.imageXML.cte.v3)
                    intent.putExtra("qrdata", data.imageXML.cte)
                    requireActivity()
                        .setResult(REQUESTCODE, intent)
                    requireActivity().finish()
                }
            } else {
                Toast.makeText(context, "Invalid Qr - Try Again", Toast.LENGTH_SHORT).show()
                val intent = Intent()
                intent.putExtra("barCode", "Invalid Qr")
                requireActivity()
                    .setResult(REQUESTCODE, intent)
                requireActivity().finish()
            }
        } catch (e: JSONException) {
            Log.e("JSON exception", e.message!!)
            e.printStackTrace()
        }


    }

    private fun openCamera() {
       /* try {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                cameraSource!!.start(surfaceView.holder)
            } else {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CAMERA),
                    REQUEST_CAMERA_PERMISSION
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }*/
    }

   /* override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CAMERA_PERMISSION && grantResults.size > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) requireActivity().finish() else openCamera()
        } else requireActivity().finish()
    }*/

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireContext(), "Camera permission granted", Toast.LENGTH_LONG).show()
                startScanning()
            } else {
                Toast.makeText(requireContext(), "Camera permission denied", Toast.LENGTH_LONG).show()
            }
        }
    }
}