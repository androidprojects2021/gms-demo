package com.example.gms_demo.ui.activity

import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.fcm_payload.fcm_payload
import com.example.gms_demo.model.fcm_payload.userdetails
import com.example.gms_demo.model.input.request.InputRequest
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import java.lang.System.exit
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private val prefs = App().sharedPrefs
    var isFromNotification: Boolean = false
    private var page: Pages? = null

    enum class Pages {
        QRSCANNER
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Objects.requireNonNull(supportActionBar)?.hide()

        LocalBroadcastManager.getInstance(this).registerReceiver(
            mMessageReceiver,
            IntentFilter("com.gms_fcm__payload")
        )

       /* if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.getString(key)
                if (key.equals("payload")) {
                    isFromNotification = true
                    val firebasePayloadData = Gson().fromJson(value, fcm_payload::class.java)
                   // showAcceptAlert()
                }
                Log.d("DetailActivity", "Key: $key Value: $value")
            }
        }
*/
        navController = findNavController(R.id.NavHost)

        onDestination()

        back.setOnClickListener { onBackPressed() }


     // var page = Pages.QRSCANNER

        if (intent != null) {
            if (!isFromNotification) {
                try {
                    Log.d("Result", "onpage: "+page)
                    page = (intent.getSerializableExtra("fragmentName") as? Pages)!!
                    loadPage(page!!)
                }catch (e:Exception){
                    Log.d("Exception", "onMain: ")
                }


                          }

            Log.d("Result 1", "onpage: "+page)


        }


    }

    private fun loadPage(page: Pages) {
        when (page) {
            Pages.QRSCANNER -> {

                findNavController(R.id.NavHost).navigate(R.id.QRscannerFragment)
            }

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }




    private fun onDestination() {
        navController.addOnDestinationChangedListener() { _, destination, _ ->
            when (destination.id) {
                R.id.PurposeFragment -> {
                    header_back.visibility = View.VISIBLE
                }
                R.id.NumberFragment -> {
                    header_back.visibility = View.VISIBLE
                }
                R.id.DepartmentFragment -> {
                    header_back.visibility = View.VISIBLE
                }
                R.id.EmployeeListFragment -> {
                    header_back.visibility = View.VISIBLE
                }
                R.id.ScanFragment -> {
                    header_back.visibility = View.VISIBLE
                }
                R.id.SuccessFragment -> {
                    header_back.visibility = View.VISIBLE
                }
                R.id.QRscannerFragment -> {
                    header_back.visibility = View.GONE
                }
                else -> {

                }
            }

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if ((navController.currentDestination?.id == R.id.PurposeFragment) ) {
            prefs.purpose = ""
            prefs.reason = ""
            prefs.phone = ""
            prefs.dept_id = ""
            prefs.employee_id = ""
            prefs.Q_id = ""
            showExitAlert(this)

        } else {
            super.onBackPressed()
        }
    }

    fun showExitAlert(context: Context?) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(context!!)
        builder.setTitle(R.string.exit)
        builder.setIcon(R.drawable.exit)
        builder.setMessage(R.string.exit_msg)
        builder.setPositiveButton(R.string.yes) { dialog, which ->
            finish()
            dialog.dismiss()
        }
        builder.setNegativeButton(R.string.no) { dialog, which -> dialog.dismiss() }
        builder.create().show()
    }


    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val firebasePayloadData: userdetails? =
                intent.getSerializableExtra("data") as userdetails?
            if (firebasePayloadData?.getAcceptedStatus().equals("2")) {
                ShowRejectAlert()
            } else if (firebasePayloadData?.getAcceptedStatus().equals("1")) {
                showAcceptAlert(firebasePayloadData?.getAcceptedStatus())
            }
        }
    }

    private fun showAcceptAlert(acceptedStatus: String?) {
        lateinit var builder: AlertDialog.Builder
        lateinit var customLayout: View
        lateinit var dialog: AlertDialog

        builder = AlertDialog.Builder(this)
        customLayout = LayoutInflater.from(this).inflate(R.layout.custom_status_dialogue, null)
        builder.setView(customLayout)
        dialog = builder.create()
        dialog.setCancelable(true)

        dialog.show()
    }

    private fun ShowRejectAlert() {
        lateinit var builder: AlertDialog.Builder
        lateinit var customLayout: View
        lateinit var dialog: AlertDialog

        builder = AlertDialog.Builder(this)
        customLayout = LayoutInflater.from(this).inflate(R.layout.reject_layout, null)
        builder.setView(customLayout)
        dialog = builder.create()
        dialog.setCancelable(true)

        dialog.show()
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onDestroy()
    }
}