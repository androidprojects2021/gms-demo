package com.example.gms_demo.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.example.gms_demo.App
import com.example.gms_demo.R
import kotlinx.android.synthetic.main.activity_main.*
import android.app.Activity





class SuccessFragment : Fragment() {
    private val prefs = App().sharedPrefs

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_success, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().back.setOnClickListener {
            prefs.reason = ""
            prefs.purpose = ""
            prefs.phone = ""
            prefs.dept_id = ""
            prefs.employee_id = ""
            prefs.Q_id = ""
            findNavController().navigate(
                R.id.action_SuccessFragment_to_PurposeFragment,
            )
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                prefs.reason = ""
                prefs.purpose = ""
                prefs.phone = ""
                prefs.dept_id = ""
                prefs.employee_id = ""
                prefs.Q_id = ""
                findNavController().navigate(
                    R.id.action_SuccessFragment_to_PurposeFragment,
                )

            }
        })

    }


}