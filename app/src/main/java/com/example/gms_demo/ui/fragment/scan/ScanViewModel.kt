package com.example.gms_demo.ui.fragment.scan

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gms_demo.App
import com.example.gms_demo.model.input.request.InputRequest
import com.example.gms_demo.model.response.purpose.ResponsePurposeList
import com.example.gms_demo.model.response.request.ResponseRequest
import kotlinx.coroutines.launch

class ScanViewModel : ViewModel() {

    private val _requestsubmitResponse = MutableLiveData<ResponseRequest>()
    val requestsubmitResponse: LiveData<ResponseRequest>
        get() = _requestsubmitResponse

    fun requesting(inputRequest: InputRequest) {
        viewModelScope.launch {
            try {

                val response = App().retrofit.submitrequest(inputRequest)
                _requestsubmitResponse.value = response

            } catch (e: Exception) {
                _requestsubmitResponse.value = ResponseRequest(status = "error")
            }
        }
    }
}