package com.example.gms_demo.ui.fragment.employelist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.input.employee.InputEmployeeList
import com.example.gms_demo.model.response.employee.EmployeesItem
import com.example.gms_demo.utils.Loader
import com.example.gms_demo.utils.isOnline
import kotlinx.android.synthetic.main.fragment_employee_list.*

/*   Created By:
 Reema 04/11/2021
 */
class EmployeeListFragment : Fragment() {
    private val prefs = App().sharedPrefs
    private lateinit var employeeListViewModel: EmployeeListViewModel
    lateinit var employeeListAdapter: EmployeeListAdapter
    private var employelist = ArrayList<EmployeesItem>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_employee_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        employeeListViewModel = ViewModelProvider(this).get(EmployeeListViewModel::class.java)
        init()
        onclick()
        onObserve()
    }


    fun init() {

        val layoutManager = GridLayoutManager(requireContext(), 4)
        recycleView_employees.layoutManager = layoutManager


        if (isOnline(requireContext())) {
/*
            Loader.showLoader(requireContext())
*/
            employeeListViewModel.getemployeelist(
                InputEmployeeList(
                    prefs.user_details?.officeId,
                    prefs.dept_id
                )
            )

        } else {
            Toast.makeText(
                requireContext(),
                "Please connect to internet",
                Toast.LENGTH_LONG
            ).show()
        }


    }

    private fun onclick() {
        employees_next.setOnClickListener {
            if (isValid()) {
                findNavController().navigate(
                    R.id.action_EmployeeListFragment_to_ScanFragment,
                )
            }
        }
    }

    private fun isValid(): Boolean {

        if (prefs.employee_id.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please select employee", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun onObserve() {
        employeeListViewModel.employelistResponse.observe(viewLifecycleOwner, Observer {
/*
            Loader.hideLoader()
*/
            if (it.status.equals("success")) {
                if (it.employees != null) {

                    employelist.clear()
                    employelist.addAll(it.employees)

                    employeeListAdapter = EmployeeListAdapter(requireContext(), employelist) {
                        prefs.employee_id = it.employeeId
                    }
                    recycleView_employees.adapter = employeeListAdapter
                }

            } else {
                Toast.makeText(
                    requireContext(),
                    "something went wrong",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}