package com.example.gms_demo.ui.activity.checkin

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.fcm_payload.fcm_payload
import com.example.gms_demo.model.fcm_payload.userdetails
import com.example.gms_demo.model.input.InputCheckin
import com.example.gms_demo.ui.activity.MainActivity
import com.example.gms_demo.utils.isOnline
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_check_in.*
import java.util.*

class CheckInActivity : AppCompatActivity() {
    private val prefs = App().sharedPrefs
    private lateinit var checkInViewModel: CheckInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in)

        Objects.requireNonNull(supportActionBar)?.hide()

        checkInViewModel = ViewModelProvider(this).get(CheckInViewModel::class.java)
        App().sharedPrefs.clearSession(this)

       /* LocalBroadcastManager.getInstance(this).registerReceiver(
            mMessageReceiver,
            IntentFilter("com.gms_fcm__payload")
        )*/



       /* if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.getString(key)
                if (key == "payload") {
                    val fcmPayout: fcm_payload = Gson().fromJson(value, fcm_payload::class.java)

                     showAcceptAlert(fcmPayout.getUserdetail().getAcceptedStatus())
                }
            }
        }*/


        onClick()
        onObserve()
    }



    override fun onResume() {
        super.onResume()

        if (prefs.Firebasetoken.isNullOrEmpty()) {
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("TAG  ", "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result
                prefs.Firebasetoken = token

                Log.e("newToken", "newToken  " + token)

            })

        }

    }


    private fun onClick() {

        chechinButton.setOnClickListener {

            if (isValid()) {
                if (isOnline(this)) {
                    checkInViewModel.checkincode(InputCheckin(editText_companyName.text.toString()))


                } else {
                    Toast.makeText(
                        this,
                        "Please connect to internet",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }



        }

    }

    private fun isValid(): Boolean {


        if (editText_companyName.text.isNullOrBlank()) {
            Toast.makeText(this, "Please Enter Code", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }


    private fun onObserve() {

        checkInViewModel.checkinResponse.observe(this, androidx.lifecycle.Observer {
            if (it.status.equals("success")) {
                val intent = Intent(applicationContext, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {

                Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()

            }
        })
    }


  /*  private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val firebasePayloadData: userdetails? =
                intent.getSerializableExtra("data") as userdetails?
            showAcceptAlert(firebasePayloadData?.getAcceptedStatus())
        }
    }*/

    private fun showAcceptAlert(acceptedStatus: String?) {
        val builder = AlertDialog.Builder(this)
        val viewGroup: ViewGroup = findViewById(android.R.id.content)
        val dialogView =
            LayoutInflater.from(this).inflate(R.layout.custom_status_dialogue, viewGroup, false)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        alertDialog.show()
    }
}