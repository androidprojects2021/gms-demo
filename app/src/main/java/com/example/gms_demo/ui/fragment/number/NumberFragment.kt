package com.example.gms_demo.ui.fragment.number

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.input.request.InputRequest
import com.example.gms_demo.utils.SharedPrefs
import kotlinx.android.synthetic.main.fragment_number.*
import java.util.ArrayList


class NumberFragment : Fragment() {
    private val prefs = App().sharedPrefs

    //private lateinit var inputRequest: InputRequest
    var inputRequest: InputRequest? = null

    private val number = ArrayList<Int>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inputRequest = InputRequest()

        init()
        onclick()
    }

    override fun onResume() {
        super.onResume()
        if (!prefs.phone.isNullOrEmpty()){
            textView_mobileNumber.text = prefs.phone

          /*  for (i in prefs.phone!!){
                number.add(i.toInt())
            }
            Log.d("NUMBER", "onResume: "+number)*/
        }
    }

    fun init() {

        val bundle = this.arguments
        if (bundle != null) {
            inputRequest = bundle.getSerializable("InputRequest") as InputRequest
        }
    }

    private fun onclick() {
        number_1.setOnClickListener { setNumberText(1) }
        number_2.setOnClickListener { setNumberText(2) }
        number_3.setOnClickListener { setNumberText(3) }
        number_4.setOnClickListener { setNumberText(4) }
        number_5.setOnClickListener { setNumberText(5) }
        number_6.setOnClickListener { setNumberText(6) }
        number_7.setOnClickListener { setNumberText(7) }
        number_8.setOnClickListener { setNumberText(8) }
        number_9.setOnClickListener { setNumberText(9) }
        number_0.setOnClickListener { setNumberText(0) }
        buttonDelete.setOnClickListener { deleteNumber() }


        num_next.setOnClickListener {
            if (number.size == 8 ) {  // || !textView_mobileNumber.text.toString().isNullOrEmpty()

                prefs.phone = textView_mobileNumber.text.toString()
                inputRequest?.mobile = textView_mobileNumber.text.toString()

                val bundle = Bundle()
                bundle.putSerializable("InputRequest", inputRequest)

                findNavController().navigate(
                    R.id.action_NumberFragment_to_DepartmentFragment, bundle
                )
            } else {
                Toast.makeText(context, "Invalid Number", Toast.LENGTH_SHORT).show()

            }


        }
    }


    private fun setNumberText(value: Int) {
        if (number.size < 8) {
            number.add(value)
        }
        setText()
    }

    private fun deleteNumber() {
        if (number.size > 0) {
            number.removeAt(number.size - 1)
            setText()
        }
    }

    private fun setText() {
        var stringNumber = ""
        for (i in number.indices) {
            stringNumber = stringNumber + number[i]
        }
        textView_mobileNumber.text = stringNumber
    }

}