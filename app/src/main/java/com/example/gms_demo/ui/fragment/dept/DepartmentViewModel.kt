package com.example.gms_demo.ui.fragment.dept

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gms_demo.App
import com.example.gms_demo.model.input.dept.InputDepartmentList
import com.example.gms_demo.model.response.dept.ResponseDepartmentsList
import com.example.gms_demo.model.response.purpose.ResponsePurposeList
import kotlinx.coroutines.launch
import java.lang.Exception

class DepartmentViewModel : ViewModel() {

    private val _departmentlistResponse = MutableLiveData<ResponseDepartmentsList>()
    val departmentlistResponse: LiveData<ResponseDepartmentsList>
        get() = _departmentlistResponse

    fun getdepartmentlist(officeId: String?) {
        viewModelScope.launch {
            try {
                val response = App().retrofit.getDepartments(InputDepartmentList((officeId)))
                _departmentlistResponse.value = response

            } catch (e: Exception) {
                _departmentlistResponse.value = ResponseDepartmentsList(status = "error")
            }
        }
    }
}