package com.example.gms_demo.ui.fragment.employelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gms_demo.App
import com.example.gms_demo.model.input.employee.InputEmployeeList
import com.example.gms_demo.model.response.employee.ResponseEmployeeList
import com.example.gms_demo.model.response.purpose.ResponsePurposeList
import kotlinx.coroutines.launch
import java.lang.Exception

/*   Created By:
 Reema 04/11/2021
 */
class EmployeeListViewModel : ViewModel() {

    private val _employelistResponse = MutableLiveData<ResponseEmployeeList>()
    val employelistResponse: LiveData<ResponseEmployeeList>
        get() = _employelistResponse

    fun getemployeelist(inputEmployeeList: InputEmployeeList) {
        viewModelScope.launch {
            try {
                val response = App().retrofit.getEmployees(inputEmployeeList)
                _employelistResponse.value = response
            } catch (e: Exception) {
                _employelistResponse.value = ResponseEmployeeList(status = "error")
            }
        }
    }
}