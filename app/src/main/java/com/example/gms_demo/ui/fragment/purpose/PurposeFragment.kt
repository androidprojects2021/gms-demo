package com.example.gms_demo.ui.fragment.purpose

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.input.request.InputRequest
import com.example.gms_demo.model.response.purpose.DetailsItem
import com.example.gms_demo.utils.Loader
import com.example.gms_demo.utils.SharedPrefs
import com.example.gms_demo.utils.isOnline
import kotlinx.android.synthetic.main.activity_check_in.*
import kotlinx.android.synthetic.main.fragment_purpose.*

/*   Created By:
 Reema 04/11/2021
 */

class PurposeFragment : Fragment() {

    private lateinit var purposeViewModel: PurposeViewModel
    lateinit var purposeListAdapter: PurposeListAdapter
    private val prefs = App().sharedPrefs
    var inputRequest: InputRequest? = null
    private var purposelist = ArrayList<DetailsItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_purpose, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        purposeViewModel = ViewModelProvider(this).get(PurposeViewModel::class.java)
        inputRequest = InputRequest()

        init()
        onclick()
        onObserve()
    }


    fun init() {
        val layoutManager = GridLayoutManager(requireContext(), 4)
        recycleView_purpose.layoutManager = layoutManager
        texviewreason.visibility = View.GONE

        if (isOnline(requireContext())) {
/*
            Loader.showLoader(requireContext())
*/
            purposeViewModel.getpurposelist()

        } else {
            Toast.makeText(
                requireContext(),
                "Please connect to internet",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun onclick() {
        next.setOnClickListener {

            if (isValid()) {
                prefs.reason = texviewreason.text.toString()
                val bundle = Bundle()
                bundle.putSerializable("InputRequest", inputRequest)
                findNavController().navigate(
                    R.id.action_PurposeFragment_to_NumberFragment, bundle
                )
            }
        }
    }

    private fun isValid(): Boolean {

        if (prefs.purpose.isNullOrEmpty()) {
            Toast.makeText(requireContext(), "Please select purpose", Toast.LENGTH_LONG).show()
            return false
        }else  if ((prefs.purpose.equals("Other")) && (texviewreason.text.toString().isNullOrEmpty())){
            Toast.makeText(requireContext(), "Please enter reason", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }


    private fun onObserve() {
        purposeViewModel.purposelistResponse.observe(viewLifecycleOwner, Observer {
/*
            Loader.hideLoader()
*/
            if (it.status.equals("success")) {
                if (it.details != null) {

                    purposelist.clear()
                    purposelist.addAll(it.details)

                    purposeListAdapter = PurposeListAdapter(requireContext(), purposelist) {
                        prefs.purpose = it.purpose
                        inputRequest?.purpose = it.purpose

                        if (it.purpose.equals("Other")){
                            texviewreason.visibility = View.VISIBLE
                        }else{texviewreason.visibility = View.GONE
                            prefs.reason = ""
                        }

                    }
                    recycleView_purpose.adapter = purposeListAdapter
                }
            } else {
                Toast.makeText(
                    requireContext(),
                    "something went wrong",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }
}