package com.example.gms_demo.ui.fragment.purpose

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gms_demo.App
import com.example.gms_demo.model.response.ResponseCheckin
import com.example.gms_demo.model.response.purpose.ResponsePurposeList
import kotlinx.coroutines.launch

/*   Created By:
 Reema 04/11/2021
 */
class PurposeViewModel:ViewModel() {

    private val _purposelistResponse = MutableLiveData<ResponsePurposeList>()
    val purposelistResponse: LiveData<ResponsePurposeList>
        get() = _purposelistResponse

    fun getpurposelist() {
        viewModelScope.launch {
            try {
                val response = App().retrofit.getpurpose_list()
                _purposelistResponse.value = response
            }catch (e:Exception){
              _purposelistResponse.value = ResponsePurposeList(status = "error")
            }
        }
    }
}