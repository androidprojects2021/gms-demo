package com.example.gms_demo.ui.fragment.purpose

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.response.purpose.DetailsItem
import com.example.gms_demo.utils.SharedPrefs

class PurposeListAdapter(
    val requireContext: Context,
    var purposeofvisitist: ArrayList<DetailsItem>,
    val onItemSelected: (DetailsItem) -> Unit
) :
    RecyclerView.Adapter<PurposeListAdapter.PlaceViewHolder>() {
    private val prefs = App().sharedPrefs

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PurposeListAdapter.PlaceViewHolder {
        return PlaceViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.custom_purpose_item_view,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PurposeListAdapter.PlaceViewHolder, position: Int) {

        holder.textviewpurpose.text = purposeofvisitist.get(position).purpose

        if (prefs.purpose.equals(purposeofvisitist.get(position).purpose)) {
            purposeofvisitist.get(position).isSelected = true
        }

        Log.d("TAG 1", "onBindViewHolder: " + prefs.purpose)
        Log.d("TAG 2", "onBindViewHolder: " + purposeofvisitist.get(position).purpose)

        if (purposeofvisitist.get(position).isSelected == true) {
            holder.textviewpurpose.setTextColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.white
                )
            )
            holder.cardview_purpose.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.red
                )
            )
        } else {
            holder.textviewpurpose.setTextColor(ContextCompat.getColor(requireContext, R.color.red))
            holder.cardview_purpose.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.white
                )
            )
        }



        holder.cardview_purpose.setOnClickListener {

            for (i in purposeofvisitist.indices) {
                purposeofvisitist.get(i).isSelected = false
            }
            purposeofvisitist.get(position).isSelected = true

            notifyDataSetChanged()
            onItemSelected.invoke(purposeofvisitist.get(position))
        }
    }

    override fun getItemCount(): Int {
        return purposeofvisitist.size
    }

    fun onrefreshdata(purposelist: ArrayList<DetailsItem>) {
        this.purposeofvisitist.clear()
        this.purposeofvisitist = purposelist
        notifyDataSetChanged()
    }

    inner class PlaceViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var textviewpurpose = itemView.findViewById<TextView>(R.id.textviewpurpose)
        var cardview_purpose = itemView.findViewById<CardView>(R.id.cardview_purpose)

    }
}