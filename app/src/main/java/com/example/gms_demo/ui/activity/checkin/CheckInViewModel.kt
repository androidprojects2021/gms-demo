package com.example.gms_demo.ui.activity.checkin

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.gms_demo.App
import com.example.gms_demo.model.input.InputCheckin
import com.example.gms_demo.model.response.ResponseCheckin
import com.example.gms_demo.utils.SharedPrefs
import kotlinx.coroutines.launch


class CheckInViewModel : ViewModel() {

    private val prefs = App().sharedPrefs

    private val _checkinResponse = MutableLiveData<ResponseCheckin>()
    val checkinResponse: LiveData<ResponseCheckin>
        get() = _checkinResponse

    fun checkincode(inputCheckin: InputCheckin) {
        viewModelScope.launch {
            try {
                val response =  App().retrofit.checkincall(inputCheckin)

                prefs.user_details = response.details
                _checkinResponse.value = response

                Log.d("CheckIn Response", "checkincode: " + response)
            } catch (e: Exception) {
                _checkinResponse.value = ResponseCheckin(status = "error",message = "Something went wrong")
            }
        }
    }

}