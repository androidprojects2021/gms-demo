package com.example.gms_demo.ui.fragment.employelist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.response.dept.DepartmentsItem
import com.example.gms_demo.model.response.employee.EmployeesItem

class EmployeeListAdapter(
    val requireContext: Context,
    val employelist: ArrayList<EmployeesItem>,
    val onItemSelected: (EmployeesItem) -> Unit
) : RecyclerView.Adapter<EmployeeListAdapter.PlaceViewHolder>() {
    private val prefs = App().sharedPrefs


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EmployeeListAdapter.PlaceViewHolder {
        return PlaceViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.custom_purpose_item_view,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: EmployeeListAdapter.PlaceViewHolder, position: Int) {
        holder.textviewpurpose.text = employelist.get(position).employeeName

        if (prefs.employee_id.equals(employelist.get(position).employeeId)) {
            employelist.get(position).isSelected = true
        }
        if (employelist.get(position).isSelected == true) {
            holder.textviewpurpose.setTextColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.white
                )
            )
            holder.cardview_purpose.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.red
                )
            )
        } else {
            holder.textviewpurpose.setTextColor(ContextCompat.getColor(requireContext, R.color.red))
            holder.cardview_purpose.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext,
                    R.color.white
                )
            )
        }
        holder.cardview_purpose.setOnClickListener {

            for (i in employelist.indices) {
                employelist.get(i).isSelected = false
            }
            employelist.get(position).isSelected = true

            notifyDataSetChanged()
            onItemSelected.invoke(employelist.get(position))
        }


    }

    override fun getItemCount(): Int {
        return employelist.size
    }

    inner class PlaceViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var textviewpurpose = itemView.findViewById<TextView>(R.id.textviewpurpose)
        var cardview_purpose = itemView.findViewById<CardView>(R.id.cardview_purpose)

    }
}