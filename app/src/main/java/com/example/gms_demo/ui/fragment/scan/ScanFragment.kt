package com.example.gms_demo.ui.fragment.scan

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.gms_demo.App
import com.example.gms_demo.R
import com.example.gms_demo.model.input.request.InputRequest
import com.example.gms_demo.model.input.request.QRDetail
import com.example.gms_demo.model.qrresult.Cte
import com.example.gms_demo.model.qrresult.QrData
import com.example.gms_demo.ui.activity.MainActivity
import com.example.gms_demo.utils.isOnline
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.google.gson.Gson
import com.google.zxing.integration.android.IntentIntegrator
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import kotlinx.android.synthetic.main.activity_check_in.*
import kotlinx.android.synthetic.main.fragment_scan.*
import org.json.JSONException
import java.util.*


class ScanFragment : Fragment() {
    private val prefs = App().sharedPrefs
    private lateinit var scanViewModel: ScanViewModel

    private val surfaceView: SurfaceView? = null
    private val textViewBarCodeValue: TextView? = null
    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private val REQUEST_CAMERA_PERMISSION = 201
    private var intentData = ""

    var qr_data: Cte? = null
    private var barcodeData = ""
    var REQUESTCODE = 111

    private var isScanned: Boolean = false


    private lateinit var builder: AlertDialog.Builder
    private lateinit var customLayout: View
    private lateinit var dialog: AlertDialog

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_scan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scanViewModel = ViewModelProvider(this).get(ScanViewModel::class.java)

        l_result.visibility = View.GONE

        init()
        onclick()
        onObserve()
    }


    private fun onObserve() {
        scanViewModel.requestsubmitResponse.observe(
                viewLifecycleOwner,
                androidx.lifecycle.Observer {
                    if (it.status.equals("success")) {

                        prefs.reason = ""
                        prefs.purpose = ""
                        prefs.phone = ""
                        prefs.dept_id = ""
                        prefs.employee_id = ""
                        prefs.Q_id = ""

                        findNavController().navigate(
                                R.id.action_ScanFragment_to_SuccessFragment,
                        )
                    } else {
                        txt_qr.text = ""
                        l_result.visibility = View.GONE
                        Toast.makeText(
                                requireContext(),
                                "something went wrong",
                                Toast.LENGTH_LONG
                        ).show()
                    }

                })
    }


    override fun onPause() {
        super.onPause()
        cameraSource?.release()
    }

    fun init() {}
    private fun onclick() {
        texviewScan.setOnClickListener {
            // Zxing Scanner
            val intentIntegrator = IntentIntegrator.forSupportFragment(this@ScanFragment)
            intentIntegrator.setPrompt("Please wait for a second")
            intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            intentIntegrator.setCameraId(1)
            intentIntegrator.setOrientationLocked(true)
            intentIntegrator.initiateScan()
            // startActivityForResuit(MainActivity.Pages.QRSCANNER, REQUESTCODE)  // Code scanner

        }

        req_submit.setOnClickListener {
            showDialogue()
        }

    }

    private fun startActivityForResuit(page: MainActivity.Pages, requestcode: Int) {

        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("fragmentName", page)
        startActivityForResult(intent, requestcode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents != null) {
                isScanned = true

                try {

                    val stringdata = intentResult.contents.toString()

                    val xmlToJson = XmlToJson.Builder(stringdata).build()
                    val data = Gson().fromJson(xmlToJson.toString(), QrData::class.java)
                    Log.d("Scan result", "receiveDetections: " + data)

                    l_result.visibility = View.VISIBLE
                    req_submit.visibility = View.GONE

                    if (data != null && data.imageXML != null && data.imageXML.cte != null) {

                        if (!data.imageXML.cte.v3?.isEmpty()!!) {


                          val  qr_data =  data.imageXML.cte

                            if (qr_data != null) {
                                if (qr_data!!.v3.equals("#11855C")) {
                                    txt_qr.text = qr_data!!.v4
                                    txt_qr.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_qr))
                                    req_submit.visibility = View.VISIBLE
                                } else {
                                    txt_qr.text = "Not allowed"
                                    txt_qr.setTextColor(ContextCompat.getColor(requireContext(), R.color.wrong_qr))
                                    req_submit.visibility = View.GONE
                                }
                            }

                        }
                    } else {
                        Toast.makeText(context, "Invalid Qr - Try Again", Toast.LENGTH_SHORT).show()
                        txt_qr.text = "Invalid QR Code"
                        txt_qr.setTextColor(ContextCompat.getColor(requireContext(), R.color.wrong_qr))
                        req_submit.visibility = View.GONE

                    }

                } catch (e: JSONException) {
                    Log.e("JSON exception", e.message!!)
                    e.printStackTrace()
                }



            } else {
                Toast.makeText(requireContext(), "QR Failed", Toast.LENGTH_SHORT).show()

            }


        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    /*  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

          super.onActivityResult(requestCode, resultCode, data)
          if (resultCode == REQUESTCODE) {

              isScanned = true

              if (data != null) {
                  barcodeData = data.getStringExtra("barCode").toString()
                  l_result.visibility = View.VISIBLE
                  req_submit.visibility = View.GONE

                  if (barcodeData.isEmpty() || barcodeData.equals("Invalid Qr")) {
                      txt_qr.text = "Invalid QR Code"
                      txt_qr.setTextColor(ContextCompat.getColor(requireContext(), R.color.wrong_qr))
                      req_submit.visibility = View.GONE
                  } else {
                      qr_data = data.getSerializableExtra("qrdata") as Cte;

                      if (qr_data != null) {
                          if (qr_data!!.v3.equals("#11855C")) {
                              txt_qr.text = qr_data!!.v4
                              txt_qr.setTextColor(ContextCompat.getColor(requireContext(), R.color.green_qr))
                              req_submit.visibility = View.VISIBLE
                          } else {
                              txt_qr.text = "Not allowed"
                              txt_qr.setTextColor(ContextCompat.getColor(requireContext(), R.color.wrong_qr))
                              req_submit.visibility = View.GONE
                          }
                      }
                  }


              }
              Log.d("QR RESULT", "onActivityResult: "+qr_data)
          }

      }*/

    private fun showDialogue() {
        builder = AlertDialog.Builder(context)
        customLayout = LayoutInflater.from(context).inflate(R.layout.custom_dialog_view, null)
        builder.setView(customLayout)
        dialog = builder.create()
        dialog.setCancelable(false)

        val submitButton = customLayout.findViewById<ImageView>(R.id.submitButton)

        submitButton.setOnClickListener {

            Log.d("Request - officeid", "showDialogue: " + prefs.user_details?.officeId)
            Log.d("Request - deviceid", "showDialogue: " + prefs.Firebasetoken)
            Log.d("Request - purpose", "showDialogue: " + prefs.purpose)
            Log.d("Request - purpose note", "showDialogue: " + prefs.reason)
            Log.d("Request - mobile", "showDialogue: " + prefs.phone)
            Log.d("Request - dept", "showDialogue: " + prefs.dept_id)
            Log.d("Request - employe", "showDialogue: " + prefs.employee_id)
            Log.d("Request - device type", "showDialogue: " + "android")

            dialog.dismiss()

            if (isOnline(requireContext())) {
                if (isScanned) {
                    if (qr_data == null) {
                        Toast.makeText(
                                context,
                                "Invalid QR Code..Please Scan Your EHTERAZ",
                                Toast.LENGTH_SHORT
                        ).show()
                    } else if (qr_data!!.v3!!.equals("#FFCA3C")) {
                        Toast.makeText(
                                context,
                                "Invalid QR Code..Please Scan Your EHTERAZ",
                                Toast.LENGTH_SHORT
                        ).show()
                    } else if (qr_data!!.v3.equals("#FF0000")) {
                        Toast.makeText(
                                context,
                                "Invalid QR Code..Please Scan Your EHTERAZ",
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        val qr_detail = QRDetail()

                        qr_detail.v1 = qr_data!!.v1.toString()
                        qr_detail.v2 = qr_data!!.v2.toString()
                        qr_detail.v3 = qr_data!!.v3.toString()
                        qr_detail.v4 = qr_data!!.v4.toString()
                        qr_detail.v5 = qr_data!!.v5.toString()
                        qr_detail.v6 = qr_data!!.v6.toString()
                        qr_detail.v7 = qr_data!!.v7.toString()
                        qr_detail.v8 = qr_data!!.v8.toString()
                        qr_detail.v9 = qr_data!!.v9.toString()
                        qr_detail.v10 = qr_data!!.v10.toString()
                        qr_detail.v11 = qr_data!!.v11.toString()
                        qr_detail.v12 = qr_data!!.v12.toString()
                        qr_detail.v13 = qr_data!!.v13.toString()

                        Log.d("Request - ScanResult", "showDialogue: " + qr_detail)
                        scanViewModel.requesting(
                                InputRequest(
                                        prefs.user_details?.officeId,
                                        prefs.Firebasetoken,
                                        prefs.purpose,
                                        prefs.reason,
                                        prefs.employee_id,
                                        prefs.phone,
                                        "android",
                                        prefs.dept_id, qr_detail
                                )
                        )

                        dialog.dismiss()
                        findNavController().navigate(
                                R.id.action_ScanFragment_to_SuccessFragment,
                        )
                    }
                }


            } else {
                Toast.makeText(
                        requireContext(),
                        "Please connect to internet",
                        Toast.LENGTH_LONG
                ).show()
            }


        }
        dialog.show()
    }


}


